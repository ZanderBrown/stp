/* stp-person.c
 *
 * Copyright 2023 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stp-person.h"

#include <gio/gio.h>

#include "stp-from-string.h"


struct _StpPerson {
  GObject  parent_instance;

  char    *name;
};

static void stp_person_from_string_iface_init (StpFromStringInterface *iface);

G_DEFINE_TYPE_WITH_CODE (StpPerson, stp_person, G_TYPE_OBJECT,
                        G_IMPLEMENT_INTERFACE (STR_TYPE_FROM_STRING,
                                               stp_person_from_string_iface_init))
                      
enum {
  PROP_0,
  PROP_NAME,
  LAST_PROP
};

static GParamSpec *pspecs[LAST_PROP] = { NULL, };


static void
stp_person_dispose (GObject *object)
{
  StpPerson *self = STR_PERSON (object);

  g_clear_pointer (&self->name, g_free);

  G_OBJECT_CLASS (stp_person_parent_class)->dispose (object);
}


static void
stp_person_set_property (GObject      *object,
                         guint         property_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  StpPerson *self = STR_PERSON (object);

  switch (property_id) {
    case PROP_NAME:
      g_set_str (&self->name, g_value_get_string (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}


static void
stp_person_get_property (GObject    *object,
                         guint       property_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  StpPerson *self = STR_PERSON (object);

  switch (property_id) {
    case PROP_NAME:
      g_value_set_string (value, self->name);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}


static void
stp_person_class_init (StpPersonClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = stp_person_dispose;
  object_class->set_property = stp_person_set_property;
  object_class->get_property = stp_person_get_property;

  pspecs[PROP_NAME] =
    g_param_spec_string ("name", NULL, NULL,
                         NULL,
                         G_PARAM_CONSTRUCT | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, LAST_PROP, pspecs);
}


static void
stp_person_parse (const char  *source,
                  GValue      *result,
                  GError     **error)
{
  /* In practice we'd want to do something interesting of course */
  g_value_take_object (result,
                       g_object_new (STR_TYPE_PERSON, "name", source, NULL));
}


static void
stp_person_from_string_iface_init (StpFromStringInterface *iface)
{
  iface->parse = stp_person_parse;
}


static void
stp_person_init (StpPerson *self)
{

}
