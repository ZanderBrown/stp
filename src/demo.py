import gi

gi.require_version("Stp", "1.0")

from gi.repository import Stp, GObject

demo = Stp.FromString.parse(Stp.Person.__gtype__, "Zed")
print(demo)


class Demo(GObject.Object, Stp.FromString):
    def do_parse(source: str):
        print(f"got {source}")
        return Demo()


demo = Stp.FromString.parse(Demo.__gtype__, "some-string")
print(demo)
