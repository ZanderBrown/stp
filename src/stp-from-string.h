/* stp-from-string.h
 *
 * Copyright 2023 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define STR_TYPE_FROM_STRING stp_from_string_get_type ()

/* can't use G_DECLARE_INTERFACE since we don't have a autoptr'able type */

GType stp_from_string_get_type (void);

typedef struct _StpFromString StpFromString;
typedef struct _StpFromStringInterface StpFromStringInterface;


/*
 * StpFromStringInterface:
 *
 * Since: 0.4
 */
struct _StpFromStringInterface {
  GTypeInterface g_iface;

  void (*parse)  (const char *source, GValue *result, GError **error);
};




void        stp_from_string_parse            (GType        type,
                                              const char  *source,
                                              GValue      *result,
                                              GError     **error);

G_END_DECLS
