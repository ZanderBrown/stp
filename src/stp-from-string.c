/* stp-from-string.c
 *
 * Copyright 2023 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stp-from-string.h"


G_DEFINE_INTERFACE (StpFromString, stp_from_string, G_TYPE_INVALID)


static void
stp_from_string_default_parse (const char              *source,
                               GValue                  *result,
                               GError                 **error)
{
  g_critical ("how'd you get here then?");
}


static void
stp_from_string_default_init (StpFromStringInterface *iface)
{
  iface->parse = stp_from_string_default_parse;
}


/**
 * stp_from_string_parse:
 * @type: the #GType to try build
 * @source: string to build from
 * @result: (out caller-allocates): the result
 */
void
stp_from_string_parse (GType        type,
                       const char  *source,
                       GValue      *result,
                       GError     **error)
{
  g_autoptr (GTypeClass) klass = g_type_class_ref (type);
  StpFromStringInterface *iface = g_type_interface_peek (klass,
                                                         STR_TYPE_FROM_STRING);

  g_return_if_fail (source != NULL);
  g_return_if_fail (result != NULL);

  g_value_init (result, type);

  if (G_UNLIKELY (!klass)) {
    g_critical ("Invalid type");
    return;
  }

  iface->parse (source, result, error);
}
